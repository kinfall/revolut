package com.moneytransfer.service;

import java.util.List;
import java.util.Objects;

import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import com.moneytransfer.dao.UserDAO;
import com.moneytransfer.dao.factory.DAOFactory;
import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.exception.ServiceException;
import com.moneytransfer.exception.UserNotFoundException;
import com.moneytransfer.model.User;
import com.moneytransfer.utils.EmailUtils;

@Named("userService")
public class UserService implements IUserService {

	private final DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);

	private final UserDAO userDao;

	public UserService() {
		super();
		this.userDao = daoFactory.getUserDAO();
	}

	public UserService(UserDAO accountDao) {
		super();
		this.userDao = accountDao;
	}

	@Override
	public List<User> getAllUsers() throws MoneyTransferExecption {
		return userDao.getAllUsers();
	}

	@Override
	public User getUserById(long userId) throws MoneyTransferExecption {

		return userDao.getUserById(userId);
	}

	@Override
	public User getUserByName(String userName) throws MoneyTransferExecption {

		if (StringUtils.isBlank(userName)) {
			throw new ServiceException("The user name should not be blank!");
		}

		return userDao.getUserByName(userName);
	}

	@Override
	public long insertUser(User user) throws MoneyTransferExecption {

		if (StringUtils.isBlank(user.getUserName())) {
			throw new ServiceException("The user name should not empty, not null and not whitespace only!");
		}
		if (!EmailUtils.isValid(user.getEmailAddress())) {
			throw new ServiceException("User email is not valid!");
		}

		User userByName = userDao.getUserByName(user.getUserName());

		if (Objects.nonNull(userByName)) {
			throw new ServiceException("The user name already exist!");
		}

		return userDao.insertUser(user);
	}

	@Override
	public int updateUser(Long userId, User user) throws MoneyTransferExecption {

		if (userId == null) {
			throw new ServiceException("The user id should not be null!");
		}

		if (StringUtils.isBlank(user.getUserName())) {
			throw new ServiceException("The user name should not empty, not null and not whitespace only!");
		}
		
		if (!EmailUtils.isValid(user.getEmailAddress())) {
			throw new ServiceException("User email is not valid!");
		}

		int updatedUser = userDao.updateUser(userId, user);

		if (updatedUser != 1) {
			throw new UserNotFoundException("Failure on delete user!");
		}

		return updatedUser;
	}

	@Override
	public int deleteUser(long userId) throws MoneyTransferExecption {
		
		int deletedUsers = userDao.deleteUser(userId);

		if (deletedUsers != 1) {
			throw new UserNotFoundException("Failure on delete user!");
		}

		return deletedUsers;
	}
}
