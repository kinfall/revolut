package com.moneytransfer.service;

import java.util.List;

import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.model.User;

public interface IUserService {

	List<User> getAllUsers() throws MoneyTransferExecption;

	User getUserById(long userId) throws MoneyTransferExecption;

	User getUserByName(String userName) throws MoneyTransferExecption;

	long insertUser(User user) throws MoneyTransferExecption;

	int updateUser(Long userId, User user) throws MoneyTransferExecption;

	int deleteUser(long userId) throws MoneyTransferExecption;

}
