package com.moneytransfer.service;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;

import com.moneytransfer.dao.AccountDAO;
import com.moneytransfer.dao.factory.DAOFactory;
import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.exception.ServiceException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.User;
import com.moneytransfer.model.UserTransaction;
import com.moneytransfer.utils.MoneyUtil;

import jersey.repackaged.com.google.common.base.Preconditions;

public class AccountService implements IAccountService {

	private final DAOFactory daoFactory = DAOFactory.getDAOFactory(DAOFactory.H2);

	private final AccountDAO accountDao;

	private final UserService userService;

	public AccountService() {
		super();
		this.accountDao = daoFactory.getAccountDAO();
		this.userService = new UserService();
	}

	public AccountService(AccountDAO accountDao) {
		super();
		this.accountDao = accountDao;
		this.userService = new UserService();
	}
	
	public AccountService(AccountDAO accountDao, UserService userService) {
		super();
		this.accountDao = accountDao;
		this.userService = userService;
	}

	@Override
	public List<Account> getAllAccounts() throws MoneyTransferExecption {
		return accountDao.getAllAccounts();
	}

	@Override
	public Account getAccountById(long accountId) throws MoneyTransferExecption {
		return accountDao.getAccountById(accountId);
	}

	@Override
	public long createAccount(Account account) throws MoneyTransferExecption {
		
		if(!MoneyUtil.isCurrencyCodeValid(account.getCurrencyCode())) {
			throw new ServiceException("Currency Code is not Valid");
		}
		
		if(StringUtils.isEmpty(account.getUserName())) {
			throw new ServiceException("The user name should be informed!");
		}
		
		if(account.getBalance() == null) {
			throw new ServiceException("The balance should be informed!");
		}
		
		User userByName = userService.getUserByName(account.getUserName());
		
		if(userByName == null) {
			throw new ServiceException("User name not found!");
		}
		
		return accountDao.createAccount(account);
	}

	@Override
	public int deleteAccountById(long accountId) throws MoneyTransferExecption {
		return accountDao.deleteAccountById(accountId);
	}

	@Override
	public int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws MoneyTransferExecption {
		return accountDao.updateAccountBalance(accountId, deltaAmount);
	}

	@Override
	public void transferAccountBalance(@Valid UserTransaction userTransaction) throws MoneyTransferExecption {

		Preconditions.checkArgument(MoneyUtil.isCurrencyCodeValid(userTransaction.getCurrencyCode()),
				"Currency Code Invalid!");

		Preconditions.checkArgument(userTransaction.getAmount().compareTo(BigDecimal.ZERO) >= 0,
				"The 'Amount' should be grather or equal then ZERO.");

		int updateCount = accountDao.transferAccountBalance(userTransaction);

		if (updateCount != 2) {
			throw new ServiceException("Transaction failed!");
		}
	}
}
