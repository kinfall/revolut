package com.moneytransfer.service;

import java.math.BigDecimal;
import java.util.List;

import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.UserTransaction;

public interface IAccountService {

	List<Account> getAllAccounts() throws MoneyTransferExecption;

	Account getAccountById(long accountId) throws MoneyTransferExecption;

	long createAccount(Account account) throws MoneyTransferExecption;

	int deleteAccountById(long accountId) throws MoneyTransferExecption;

	int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws MoneyTransferExecption;

	void transferAccountBalance(UserTransaction userTransaction) throws MoneyTransferExecption;
}
