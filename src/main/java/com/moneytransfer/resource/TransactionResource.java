package com.moneytransfer.resource;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.model.UserTransaction;
import com.moneytransfer.service.AccountService;
import com.moneytransfer.service.IAccountService;

@Path("/transactions")
@Produces(MediaType.APPLICATION_JSON)
public class TransactionResource {

	private final IAccountService accountService;
	
	public TransactionResource() {
		super();
		this.accountService = new AccountService();
	}

	public TransactionResource(IAccountService accountService) {
		super();
		this.accountService = accountService;
	}

	@POST
	public Response transferFund(UserTransaction transaction) throws MoneyTransferExecption {

		accountService.transferAccountBalance(transaction);
		
		return Response.status(Response.Status.OK).build();
	}
}
