package com.moneytransfer.resource;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.exception.ServiceException;
import com.moneytransfer.model.Account;
import com.moneytransfer.service.AccountService;
import com.moneytransfer.service.IAccountService;
import com.moneytransfer.utils.MoneyUtil;

/**
 * Account Service 
 */
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
public class AccountResource {
	
    private static Logger log = Logger.getLogger(AccountResource.class);

    private final IAccountService accountService;
    
    public AccountResource() {
		super();
		this.accountService = new AccountService();
	}
    
	public AccountResource(IAccountService accountService) {
		super();
		this.accountService = accountService;
	}

	/**
     * Find all accounts
     * @return
     * @throws MoneyTransferExecption
     */
    @GET
    @Path("/all")
    public List<Account> getAllAccounts() throws MoneyTransferExecption {
        return accountService.getAllAccounts();
    }

    /**
     * Find by account id
     * @param accountId
     * @return
     * @throws MoneyTransferExecption
     */
    @GET
    @Path("/{accountId}")
    public Account getAccount(@PathParam("accountId") long accountId) throws MoneyTransferExecption {
        return accountService.getAccountById(accountId);
    }
    
    /**
     * Find balance by account Id
     * @param accountId
     * @return
     * @throws MoneyTransferExecption
     */
    @GET
    @Path("/{accountId}/balance")
    public BigDecimal getBalance(@PathParam("accountId") long accountId) throws MoneyTransferExecption {
        final Account account = accountService.getAccountById(accountId);

        if(account == null){
            throw new WebApplicationException("Account not found", Response.Status.NOT_FOUND);
        }
        return account.getBalance();
    }
    
    /**
     * Create Account
     * @param account
     * @return
     * @throws MoneyTransferExecption
     */
    @POST
    @Path("/")
    public Account createAccount(Account account) throws MoneyTransferExecption {
        
    	final long accountId = accountService.createAccount(account);
      
        return accountService.getAccountById(accountId);
    }

    /**
     * Deposit amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws MoneyTransferExecption
     */
    @PUT
    @Path("/{accountId}/deposit/{amount}")
    public Account deposit(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws MoneyTransferExecption {

    	if (amount.compareTo(MoneyUtil.zeroAmount) <= 0){
            throw new ServiceException("Invalid deposit amount!");
        }
    	
        accountService.updateAccountBalance(accountId,amount.setScale(4, RoundingMode.HALF_EVEN));
        
        return accountService.getAccountById(accountId);
    }

    /**
     * Withdraw amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws MoneyTransferExecption
     */
    @PUT
    @Path("/{accountId}/withdraw/{amount}")
    public Account withdraw(@PathParam("accountId") long accountId,@PathParam("amount") BigDecimal amount) throws MoneyTransferExecption {

		if (amount.compareTo(MoneyUtil.zeroAmount) <= 0){
            throw new ServiceException("Invalid withdraw amount!");
        }
    	
        BigDecimal delta = amount.negate();
        
        if (log.isDebugEnabled()) {
            log.debug("Withdraw service: delta change to account  " + delta + " Account ID = " +accountId);
        }
        
        accountService.updateAccountBalance(accountId,delta.setScale(4, RoundingMode.HALF_EVEN));
        
        return accountService.getAccountById(accountId);
    }


    /**
     * Delete amount by account Id
     * @param accountId
     * @param amount
     * @return
     * @throws MoneyTransferExecption
     */
    @DELETE
    @Path("/{accountId}")
    public Response deleteAccount(@PathParam("accountId") long accountId) throws MoneyTransferExecption {
        int deleteCount = accountService.deleteAccountById(accountId);
        if (deleteCount == 1) {
            return Response.status(Response.Status.OK).build();
        } else {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }

}
