package com.moneytransfer.resource;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.moneytransfer.exception.MoneyTransferExecption;
import com.moneytransfer.model.User;
import com.moneytransfer.service.IUserService;
import com.moneytransfer.service.UserService;

@Path("/users")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	private final IUserService userService;

	private final Logger log; 

	public UserResource() {
		super();
		this.userService = new UserService();
		this.log = Logger.getLogger(UserResource.class);
	}

	public UserResource(IUserService userService, Logger log) {
		super();
		this.userService = userService;
		this.log = log;
	}

	/**
	 * Find by userName
	 * 
	 * @param userName
	 * @return
	 * @throws MoneyTransferExecption
	 */
	@GET
	@Path("/{userName}")
	public User getUserByName(@PathParam("userName") String userName) throws MoneyTransferExecption {
		
		if (log.isDebugEnabled()) {
			log.debug("Request Received for get User by Name " + userName);
		}
		
		final User user = userService.getUserByName(userName);

		if (user == null) {
			throw new WebApplicationException("User Not Found", Response.Status.NOT_FOUND);
		}

		return user;
	}

	/**
	 * Find by all
	 * 
	 * @param USER_NAME
	 * @return
	 * @throws MoneyTransferExecption
	 */
	@GET
	@Path("/all")
	public List<User> getAllUsers() throws MoneyTransferExecption {
		return userService.getAllUsers();
	}

	/**
	 * Create User
	 * 
	 * @param user
	 * @return
	 * @throws MoneyTransferExecption
	 */
	@POST
	@Path("/")
	public User createUser(User user) throws MoneyTransferExecption {
		
		final long uId = userService.insertUser(user);
		
		return userService.getUserById(uId);
	}

	/**
	 * Find by User Id
	 * 
	 * @param userId
	 * @param user
	 * @return
	 * @throws MoneyTransferExecption
	 */
	@PUT
	@Path("/{userId}")
	public Response updateUser(@PathParam("userId") long userId, User user) throws MoneyTransferExecption {
		
		userService.updateUser(userId, user);

		return Response.status(Response.Status.OK).build();
	}

	/**
	 * Delete by User Id
	 * 
	 * @param userId
	 * @return
	 * @throws MoneyTransferExecption
	 */
	@DELETE
	@Path("/{userId}")
	public Response deleteUser(@PathParam("userId") long userId) throws MoneyTransferExecption {
		
		userService.deleteUser(userId);
		
		return Response.status(Response.Status.OK).build();
	}
}
