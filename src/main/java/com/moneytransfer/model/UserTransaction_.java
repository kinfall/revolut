package com.moneytransfer.model;

public final class UserTransaction_ {

	public static final String CURRENCY_CODE = "currencyCode";
	public static final String AMOUNT = "amount";
	public static final String FROM_ACCOUNT_ID = "fromAccountId";
	public static final String TO_ACCOUNT_ID = "toAccountId";

	private UserTransaction_() {
		super();
	}
}
