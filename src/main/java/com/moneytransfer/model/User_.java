package com.moneytransfer.model;

public final class User_ {

    public static final String USER_ID = "UserId";
    public static final String USER_NAME = "UserName";
    public static final String EMAIL_ADDRESS = "EmailAddress";
	
    private User_() {
		super();
	}
}
