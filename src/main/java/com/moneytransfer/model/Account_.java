package com.moneytransfer.model;

//@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
//@StaticMetamodel(Author.class)
public final class Account_ {

	
	    public static final String ACCOUNT_ID = "AccountId";
	    public static final String USER_NAME = "UserName";
	    public static final String BALANCE = "Balance";
	    public static final String CURRENCY_CODE = "CurrencyCode";
		
	    private Account_() {
			super();
		}
}
