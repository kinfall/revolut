package com.moneytransfer.mapper;

import org.apache.log4j.Logger;

import com.moneytransfer.exception.ServiceException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ServiceExceptionMapper implements ExceptionMapper<ServiceException> {

	private final Logger log ;

	public ServiceExceptionMapper() {
		super();
		this.log = Logger.getLogger(ServiceExceptionMapper.class);
	}
	
	public ServiceExceptionMapper(Logger log ) {
		super();
		this.log = log;
	}

	public Response toResponse(ServiceException serviceException) {
		if (log.isDebugEnabled()) {
			log.debug("Mapping exception to Response....");
		}
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(serviceException.getMessage());

		return Response.status(Response.Status.BAD_REQUEST)
					   .entity(errorResponse)
					   .type(MediaType.APPLICATION_JSON)
					   .build();
	}
}
