package com.moneytransfer.mapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.log4j.Logger;

import com.moneytransfer.exception.DataAccessException;

public class DataAccessExceptionMapper  implements ExceptionMapper<DataAccessException> {

	private final Logger log ;

	public DataAccessExceptionMapper() {
		super();
		this.log = Logger.getLogger(DataAccessExceptionMapper.class);
	}
	
	public DataAccessExceptionMapper(Logger log ) {
		super();
		this.log = log;
	}

	public Response toResponse(DataAccessException exception) {
		if (log.isDebugEnabled()) {
			log.debug("Mapping exception to Response....");
		}
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(exception.getMessage());

		return Response.status(Response.Status.BAD_REQUEST)
					   .entity(errorResponse)
					   .type(MediaType.APPLICATION_JSON)
					   .build();
	}
}
