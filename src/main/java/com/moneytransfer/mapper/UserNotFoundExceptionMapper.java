package com.moneytransfer.mapper;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.log4j.Logger;

import com.moneytransfer.exception.UserNotFoundException;

public class UserNotFoundExceptionMapper  implements ExceptionMapper<UserNotFoundException> {

	private final Logger log ;

	public UserNotFoundExceptionMapper() {
		super();
		this.log = Logger.getLogger(ServiceExceptionMapper.class);
	}
	
	public UserNotFoundExceptionMapper(Logger log ) {
		super();
		this.log = log;
	}

	public Response toResponse(UserNotFoundException exception) {
		if (log.isDebugEnabled()) {
			log.debug("Mapping exception to Response....");
		}
		
		ErrorResponse errorResponse = new ErrorResponse();
		errorResponse.setErrorCode(exception.getMessage());

		return Response.status(Response.Status.NOT_FOUND)
					   .entity(errorResponse)
					   .type(MediaType.APPLICATION_JSON)
					   .build();
	}
}
