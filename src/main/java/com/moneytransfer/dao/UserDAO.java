package com.moneytransfer.dao;

import java.util.List;

import com.moneytransfer.exception.DataAccessException;
import com.moneytransfer.model.User;

public interface UserDAO {
	
	List<User> getAllUsers() throws DataAccessException;

	User getUserById(long userId) throws DataAccessException;

	User getUserByName(String userName) throws DataAccessException;

	long insertUser(User user) throws DataAccessException;

	int updateUser(Long userId, User user) throws DataAccessException;

	int deleteUser(long userId) throws DataAccessException;

}
