package com.moneytransfer.dao;

import java.math.BigDecimal;
import java.util.List;

import com.moneytransfer.exception.DataAccessException;
import com.moneytransfer.model.Account;
import com.moneytransfer.model.UserTransaction;

public interface AccountDAO {

	List<Account> getAllAccounts() throws DataAccessException;

	Account getAccountById(long accountId) throws DataAccessException;

	long createAccount(Account account) throws DataAccessException;

	int deleteAccountById(long accountId) throws DataAccessException;

	int updateAccountBalance(long accountId, BigDecimal deltaAmount) throws DataAccessException;

	int transferAccountBalance(UserTransaction userTransaction) throws DataAccessException;
}
