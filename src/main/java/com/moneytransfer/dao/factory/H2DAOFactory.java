package com.moneytransfer.dao.factory;

import com.moneytransfer.dao.AccountDAO;
import com.moneytransfer.dao.UserDAO;
import com.moneytransfer.dao.impl.AccountDAOImpl;
import com.moneytransfer.dao.impl.UserDAOImpl;
import com.moneytransfer.utils.Utils;

import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;
import org.h2.tools.RunScript;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * H2 DAO
 */
public class H2DAOFactory extends DAOFactory {

	private static Logger log = Logger.getLogger(H2DAOFactory.class);

	private static final String H2_DRIVER = Utils.getStringProperty("h2_driver");
	private static final String H2_CONNECTION_URL = Utils.getStringProperty("h2_connection_url");
	private static final String H2_USER = Utils.getStringProperty("h2_user");
	private static final String H2_PASSWORD = Utils.getStringProperty("h2_password");

	private final UserDAOImpl userDAO = new UserDAOImpl();
	private final AccountDAOImpl accountDAO = new AccountDAOImpl();

	public H2DAOFactory() {
		DbUtils.loadDriver(H2_DRIVER);
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection(H2_CONNECTION_URL, H2_USER, H2_PASSWORD);
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public AccountDAO getAccountDAO() {
		return accountDAO;
	}

	@Override
	public void populateTestData() {
		log.info("Populating Test User Table and data ..... ");
		
		try (Connection conn = H2DAOFactory.getConnection()){
			
			RunScript.execute(conn, new FileReader("src/test/resources/demo.sql"));
			
		} catch (SQLException e) {
			log.error("populateTestData(): Error populating user data: ", e);
			throw new RuntimeException(e);
		} catch (FileNotFoundException e) {
			log.error("populateTestData(): Error finding test script file ", e);
			throw new RuntimeException(e);
		}
	}
}
