package com.moneytransfer.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.moneytransfer.dao.UserDAO;
import com.moneytransfer.dao.factory.H2DAOFactory;
import com.moneytransfer.exception.DataAccessException;
import com.moneytransfer.model.User;
import com.moneytransfer.model.User_;

public class UserDAOImpl implements UserDAO {

	private static Logger log = Logger.getLogger(UserDAOImpl.class);
	private static final String SQL_GET_USER_BY_ID = "SELECT * FROM User WHERE UserId = ? ";
	private static final String SQL_GET_ALL_USERS = "SELECT * FROM User";
	private static final String SQL_GET_USER_BY_NAME = "SELECT * FROM User WHERE UserName = ? ";
	private static final String SQL_INSERT_USER = "INSERT INTO User (UserName, EmailAddress) VALUES (?, ?)";
	private static final String SQL_UPDATE_USER = "UPDATE User SET UserName = ?, EmailAddress = ? WHERE UserId = ? ";
	private static final String SQL_DELETE_USER_BY_ID = "DELETE FROM User WHERE UserId = ? ";

	/**
	 * Find all users
	 */
	public List<User> getAllUsers() throws DataAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<User> users = new ArrayList<>();
		try {
			conn = H2DAOFactory.getConnection();
			stmt = conn.prepareStatement(SQL_GET_ALL_USERS);
			rs = stmt.executeQuery();
			while (rs.next()) {
				User u = new User(rs.getLong(User_.USER_ID), rs.getString(User_.USER_NAME),
						rs.getString(User_.EMAIL_ADDRESS));

				users.add(u);
				if (log.isDebugEnabled())
					log.debug("getAllUsers() Retrieve User: " + u);
			}
			return users;
		} catch (SQLException e) {
			throw new DataAccessException("Error reading user data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
	}

	/**
	 * Find user by userId
	 */
	public User getUserById(long userId) throws DataAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;

		User user = null;

		try {
			conn = H2DAOFactory.getConnection();
			stmt = conn.prepareStatement(SQL_GET_USER_BY_ID);
			stmt.setLong(1, userId);

			rs = stmt.executeQuery();

			if (rs.next()) {
				user = new User(rs.getLong(User_.USER_ID), rs.getString(User_.USER_NAME),
						rs.getString(User_.EMAIL_ADDRESS));

				if (log.isDebugEnabled()) {
					log.debug("getUserById(): Retrieve User: " + user);
				}
			}
			return user;
		} catch (SQLException e) {
			throw new DataAccessException("Error reading user data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
	}

	/**
	 * Find user by userName
	 */
	public User getUserByName(String userName) throws DataAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		User user = null;
		try {
			conn = H2DAOFactory.getConnection();
			stmt = conn.prepareStatement(SQL_GET_USER_BY_NAME);
			stmt.setString(1, userName);

			rs = stmt.executeQuery();

			if (rs.next()) {

				user = new User(rs.getLong(User_.USER_ID), rs.getString(User_.USER_NAME),
						rs.getString(User_.EMAIL_ADDRESS));

				if (log.isDebugEnabled()) {
					log.debug("Retrieve User: " + user);
				}
			}
			return user;
		} catch (SQLException e) {
			throw new DataAccessException("Error reading user data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, rs);
		}
	}

	/**
	 * Save User
	 */
	public long insertUser(User user) throws DataAccessException {

		Connection conn = null;
		PreparedStatement stmt = null;
		ResultSet generatedKeys = null;

		try {
			conn = H2DAOFactory.getConnection();

			stmt = conn.prepareStatement(SQL_INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, StringUtils.trim(user.getUserName()));
			stmt.setString(2, StringUtils.trim(user.getEmailAddress()));

			int affectedRows = stmt.executeUpdate();

			if (affectedRows == 0) {
				log.error("insertUser(): Creating user failed, no rows affected." + user);
				throw new DataAccessException("Users Cannot be created");
			}

			generatedKeys = stmt.getGeneratedKeys();

			if (generatedKeys.next()) {
				return generatedKeys.getLong(1);
			} else {
				log.error("insertUser():  Creating user failed, no ID obtained." + user);
				throw new DataAccessException("Users Cannot be created");
			}

		} catch (SQLException e) {
			log.error("Error Inserting User :" + user);
			throw new DataAccessException("Error creating user data", e);
		} finally {
			DbUtils.closeQuietly(conn, stmt, generatedKeys);
		}

	}

	/**
	 * Update User
	 */
	public int updateUser(Long userId, User user) throws DataAccessException {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = H2DAOFactory.getConnection();
			stmt = conn.prepareStatement(SQL_UPDATE_USER);
			stmt.setString(1, user.getUserName());
			stmt.setString(2, user.getEmailAddress());
			stmt.setLong(3, userId);

			return stmt.executeUpdate();
		} catch (SQLException e) {
			log.error("Error Updating User :" + user);
			throw new DataAccessException("Error update user data", e);
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(stmt);
		}
	}

	/**
	 * Delete User
	 */
	public int deleteUser(long userId) throws DataAccessException {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = H2DAOFactory.getConnection();
			stmt = conn.prepareStatement(SQL_DELETE_USER_BY_ID);
			stmt.setLong(1, userId);

			return stmt.executeUpdate();
			
		} catch (SQLException e) {
			log.error("Error Deleting User :" + userId);
			throw new DataAccessException("Error Deleting User ID:" + userId, e);
		} finally {
			DbUtils.closeQuietly(conn);
			DbUtils.closeQuietly(stmt);
		}
	}
}
