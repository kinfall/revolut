package com.moneytransfer.exception;

public class UserNotFoundException extends ServiceException {

	private static final long serialVersionUID = 1L;

	public UserNotFoundException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public UserNotFoundException(String msg) {
		super(msg);
	}
	
	
}
