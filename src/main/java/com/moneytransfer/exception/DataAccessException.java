package com.moneytransfer.exception;

public class DataAccessException extends MoneyTransferExecption {

	private static final long serialVersionUID = 1L;

	public DataAccessException(String msg) {
		super(msg);
	}

	public DataAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}

}
