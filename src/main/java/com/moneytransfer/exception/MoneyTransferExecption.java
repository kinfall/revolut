package com.moneytransfer.exception;

public class MoneyTransferExecption extends Exception {

	private static final long serialVersionUID = 1L;

	public MoneyTransferExecption() {
		super();
	}

	public MoneyTransferExecption(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MoneyTransferExecption(String message, Throwable cause) {
		super(message, cause);
	}

	public MoneyTransferExecption(String message) {
		super(message);
	}

	public MoneyTransferExecption(Throwable cause) {
		super(cause);
	}
}
