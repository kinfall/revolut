package com.moneytransfer.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.List;
import java.util.Set;

import javax.money.CurrencyUnit;

import org.apache.log4j.Logger;

/**
 * Utilities class to operate on money
 */
public enum MoneyUtil {

	INSTANCE;

	static Logger log = Logger.getLogger(MoneyUtil.class);

	// zero amount with scale 4 and financial rounding mode
	public static final BigDecimal zeroAmount = new BigDecimal(0).setScale(4, RoundingMode.HALF_EVEN);

	/**
	 * @param inputCcyCode String Currency code to be validated
	 * @return true if currency code is valid ISO code, false otherwise
	 */
	public static boolean isCurrencyCodeValid(String inputCcyCode) {

		Set<Currency> availableCurrencies = Currency.getAvailableCurrencies();

		return availableCurrencies.contains(Currency.getInstance(inputCcyCode));
	}
}
