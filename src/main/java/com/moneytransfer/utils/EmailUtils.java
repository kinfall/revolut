package com.moneytransfer.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

public final class EmailUtils {
	
	public static boolean isValid(String email) {
		
		if(StringUtils.isBlank(email)) {return false;}
		
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		 
		Pattern pattern = Pattern.compile(regex);
		 
	    Matcher matcher = pattern.matcher(email);
	    return  matcher.matches();
	}

	private EmailUtils() {
		super();
	}
}
