package com.moneytransfer;

import org.glassfish.hk2.utilities.binding.AbstractBinder;

import com.moneytransfer.service.IUserService;
import com.moneytransfer.service.UserService;

public class ApplicationBinder extends AbstractBinder{

	@Override
	protected void configure() {
		bind(IUserService.class).to(UserService.class);

	}

}
