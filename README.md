# Money transfer Rest API

A Java RESTful API for money transfers between users accounts

### Technologies
- JAX-RS API
- H2 in memory database
- Log4j
- Jetty Container (for Test and Demo app)
- Apache HTTP Client


### How to run
```sh
mvn exec:java
```

Application starts a jetty server on localhost port 8080 An H2 in memory database initialized with some sample user and account data To view

- http://localhost:8080/users/test1
- http://localhost:8080/users/test2
- http://localhost:8080/accounts/1
- http://localhost:8080/accounts/2

### Available Services

| HTTP METHOD | PATH | USAGE |
| -----------| ------ | ------ |
| GET | /users/{userName} | get user by user name | 
| GET | /users/all | get all users | 
| PUT | /users/{userId} | update a user | 
| POST | /users | create new user | 
| DELETE | /users/{userId} | remove user | 
| GET | /accounts/{accountId} | get account by accountId | 
| GET | /accounts/all | get all accounts | 
| GET | /accounts/{accountId}/balance | get account balance by accountId | 
| POST | /accounts | create a new account
| DELETE | /accounts/{accountId} | remove account by accountId | 
| PUT | /accounts/{accountId}/withdraw/{amount} | withdraw money from account | 
| PUT | /accounts/{accountId}/deposit/{amount} | deposit money to account | 
| POST | /transactions | perform transaction between 2 user accounts | 

### Http Status
- 200 OK: The request has succeeded
- 400 Bad Request: The request could not be understood by the server 
- 404 Not Found: The requested resource cannot be found

### Sample JSON for User and Account
##### User : 
```sh
{  
  "userName":"test1",
  "emailAddress":"test1@gmail.com"
} 
```
##### User Account: : 

```sh
{  
   "userName":"test1",
   "balance":10.0000,
   "currencyCode":"GBP"
} 
```

#### User Transaction:
```sh
{  
   "currencyCode":"EUR",
   "amount":100000.0000,
   "fromAccountId":1,
   "toAccountId":2
}
```
